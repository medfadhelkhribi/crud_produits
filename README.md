# crud Produits
- Install Django module: `pip install django`
- Run backend server: ` python .\manage.py runserver`
- install Django rest framework `pip install djangorestframework`
- install Django CORS headers module `pip install django-cors-headers`


# Fichiers:
>> Le fichier ____init__.py__ est simplement un fichier vide qui indique que le dossier donné est un projet Python ou un module Python.

>> __asgi.py__ est le point d'entrée pour les serveurs web compatibles asgi.

>> __wsgi.py__ est le point d'entrée pour les serveurs web compatibles wsgi.

>> Le fichier __urls.py__ contient toutes les déclarations d'URL nécessaires pour ce projet.

>> Le fichier __settings.py__ contient tous les paramètres ou configurations nécessaires pour le projet.

>> __manage.py__ est une utilité en ligne de commande qui aide à interagir avec le projet Django.

## Base de donnèes: (SqLite)

![img.png](img/imgdb.png)

## APIs:

- GET :

![GET](img/img-get.png)

- SEARCH :

![img.png](img/img-search.png)

- GET ONE PRODUCT:

![img.png](img/img-get-1.png)

- ADD :

![img.png](img/img-add.png)

- PUT :

![img2.png](img/img-update.png)

- DELETE

![img.png](img/img-delete.png)

# Front End (React.Js)
- Installer les modules: `npm i`
- Run server : `ng serve`